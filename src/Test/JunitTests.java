package Test;

import Model.*;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;
public class JunitTests {
    private ArrayList<Player> playerArrayList = new ArrayList<>();
    private final String firstName1 = "Lamine";
    private final String firstName2 = "Fatoumata";
    @Test
    public void testFixedChoice(){
        Player player1 = new Player(firstName1, new FixedChoice(PlayerMove.SCISSORS));
        Player player2 = new Player(firstName2, new FixedChoice(PlayerMove.PAPER));
        Game game = new Game(player1, player2, 10);
        playerArrayList = game.launchGame();
        assertEquals(playerArrayList.get(0).getScore(),10);
        assertEquals(playerArrayList.get(1).getScore(),0);

        player2.setStrategy(new FixedChoice(PlayerMove.SCISSORS));
        game.setPlayer1(player1);
        game.setPlayer2(player2);
        playerArrayList = game.launchGame();
        assertEquals(playerArrayList.get(0).getScore(),10);
        assertEquals(playerArrayList.get(1).getScore(),0);

        player1.setStrategy(new FixedChoice(PlayerMove.PAPER));
        game.setPlayer1(player1);
        game.setPlayer2(player2);
        playerArrayList = game.launchGame();
        assertEquals(playerArrayList.get(0).getScore(), playerArrayList.get(1).getScore());
    }
    @Test
    public void test_next(){
        PlayerMove last = PlayerMove.ROCK;
        PlayerMove current;
        for (int i = 0; i< 5; i++){
            current = last;
            last = current.next();
            assertNotEquals(current, last);
            System.out.println("current = " +current +" last = " +last);
        }
    }
    @Test
    public void test_IncrementedChoice(){
        IncrementalChoice incrementalChoice1 = new IncrementalChoice(PlayerMove.SCISSORS);
        IncrementalChoice incrementalChoice2 = new IncrementalChoice(PlayerMove.PAPER);
        Player player1 = new Player(firstName1, incrementalChoice1);
        Player player2 = new Player(firstName2, incrementalChoice2);
        Game game = new Game(player1, player2, 4);
        playerArrayList = game.launchGame();
        assertEquals(playerArrayList.get(0).getScore(),4); //This won't pass when you test RPSLS
        assertEquals(playerArrayList.get(1).getScore(),0);

        player1.setStrategy(incrementalChoice2);
        game.setPlayer1(player1);
        game.setPlayer2(player2);
        playerArrayList = game.launchGame();
        assertEquals(playerArrayList.get(0).getScore(),playerArrayList.get(1).getScore());
    }
    @Test
    public void test_getRandomMove(){
        System.out.println(PlayerMove.getRandomMove());
        assertNotNull(PlayerMove.getRandomMove());
    }
    @Test
    public void test_isValidMove(){
        assertTrue(PlayerMove.isValid("0"));
        assertFalse(PlayerMove.isValid("-1"));
        assertFalse(PlayerMove.isValid("a"));
        assertFalse(PlayerMove.isValid("10ab0"));
    }
    @Test
    public void test_getDecisionArray(){
        /*One can update this test to retrieve RPSLS_array. To do this,
        update the enum class list by un-commenting Lizard and spock
        and update also the decision table accordingly.
         */
        PlayerMove playerMove = PlayerMove.ROCK;
        final int[][] decision = {
                {0, -1, 1},
                {1, 0, -1},
                {-1, 1, 0}
        };
        final int[][] retrieveDecision = playerMove.getDecisionArray();
        assertEquals(decision, retrieveDecision);
    }
}
