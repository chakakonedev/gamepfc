package Test;

import Model.*;

import java.util.ArrayList;

public class FunctionalTests {
    private final String firstName1 = "Lamine";
    private final String firstName2 = "Fatoumata";
    private ArrayList<Player> playerArrayList = new ArrayList<>();

    public ArrayList<Player> testFixedChoice(){
        PlayerMove movePlayer1 = PlayerMove.SCISSORS;
        PlayerMove movePlayer2 = PlayerMove.PAPER;
        FixedChoice fixedChoice1 = new FixedChoice(movePlayer1);
        FixedChoice fixedChoice2 = new FixedChoice(movePlayer2);
        Player player1 = new Player(firstName1, fixedChoice1);
        Player player2 = new Player(firstName2, fixedChoice2);
        Game game = new Game(player1, player2, 10);
        playerArrayList = game.launchGame();
        return playerArrayList;
    }
    public ArrayList<Player> testFixedChoice1(){
        Player player1 = new Player(firstName1, new FixedChoice(PlayerMove.SCISSORS));
        Player player2 = new Player(firstName2, new FixedChoice(PlayerMove.SCISSORS));

        Game game2 = new Game(player1, player2);
        playerArrayList = game2.launchGame();
        return playerArrayList;
    }

    public ArrayList<Player> testIncrementalChoice(){
        IncrementalChoice incrementalChoice1 = new IncrementalChoice(PlayerMove.SCISSORS);
        IncrementalChoice incrementalChoice2 = new IncrementalChoice(PlayerMove.PAPER);
        Player player1 = new Player(firstName1, incrementalChoice1);
        Player player2 = new Player(firstName2, incrementalChoice2);
        Game game = new Game(player1, player2, 4);
        playerArrayList = game.launchGame();
        return playerArrayList;
    }
    public ArrayList<Player> testIncrementalChoice1(){
        Player player1 = new Player(firstName1, new IncrementalChoice());
        Player player2 = new Player(firstName2, new IncrementalChoice(PlayerMove.PAPER));
        Game game = new Game(player1, player2, 4);
        playerArrayList = game.launchGame();
        return playerArrayList;
    }
    public ArrayList<Player> testMixedFixIncChoices(){
        FixedChoice fixedChoice1 = new FixedChoice(PlayerMove.ROCK);
        IncrementalChoice incrementalChoice2 = new IncrementalChoice(PlayerMove.PAPER);
        Player player1 = new Player(firstName1, fixedChoice1);
        Player player2 = new Player(firstName2, incrementalChoice2);
        Game game = new Game(player1, player2, 4);
        playerArrayList = game.launchGame();
        return playerArrayList;
    }
    public ArrayList<Player> testRandonChoice(){
        RandomChoice randomChoice = new RandomChoice();
        Player player1 = new Player(firstName1, randomChoice);
        Player player2 = new Player(firstName2, randomChoice);
        Game game = new Game(player1, player2, 4);
        playerArrayList = game.launchGame();
        return playerArrayList;
    }
    public ArrayList<Player> testMixedFixRandonChoices(){
        FixedChoice fixedChoice1 = new FixedChoice(PlayerMove.ROCK);
        RandomChoice randomChoice = new RandomChoice();
        Player player1 = new Player(firstName1, fixedChoice1);
        Player player2 = new Player(firstName2, randomChoice);
        Game game = new Game(player1, player2, 4);
        playerArrayList = game.launchGame();
        return playerArrayList;
    }
    public ArrayList<Player> testMixedIncRandonChoices(){
        RandomChoice randomChoice = new RandomChoice();
        Player player1 = new Player(firstName1, new IncrementalChoice(PlayerMove.SCISSORS));
        Player player2 = new Player(firstName2, randomChoice);
        Game game = new Game(player1, player2, 4);
        playerArrayList = game.launchGame();
        return playerArrayList;
    }
    public ArrayList<Player> testKeyboardChoice(){
        Player player1 = new Player(firstName1, new KeyboardChoice());
        Player player2 = new Player(firstName2, new KeyboardChoice());
        Game game = new Game(player1, player2, 4);
        playerArrayList = game.launchGame();
        return playerArrayList;
    }
    public ArrayList<Player> testPlayerVsComputer()
    {
        Player player1 = new Player(firstName1, new FixedChoice(PlayerMove.ROCK));
        Game game = new Game(player1, 4);
        playerArrayList = game.launchGame();
        return playerArrayList;
    }
    public ArrayList<Player> testPlayerVsComputer1()
    {
        Player player1 = new Player(firstName1, new FixedChoice(PlayerMove.ROCK));
        Game game = new Game(player1);
        playerArrayList = game.launchGame();
        return playerArrayList;
    }
    public ArrayList<Player> testComputerVsComputer()
    {
        Game game = new Game();
        playerArrayList = game.launchGame();
        return playerArrayList;
    }
    public ArrayList<Player> testComputerVsComputer1()
    {
        Game game = new Game(5);
        playerArrayList = game.launchGame();
        return playerArrayList;
    }
}
