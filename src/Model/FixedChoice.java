package Model;

public class FixedChoice implements Strategy {
    private PlayerMove moveChosen;

    public FixedChoice(PlayerMove moveChosen) {
        this.moveChosen = moveChosen;
    }

    public void setMoveChosen(PlayerMove moveChosen) {
        this.moveChosen = moveChosen;
    }

    @Override
    public PlayerMove chooseTheMove() {
        return this.moveChosen;
    }
}
