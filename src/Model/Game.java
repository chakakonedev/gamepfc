package Model;

import java.util.ArrayList;


public class Game {
    private Player player1;
    private Player player2;
    private int numberGames;

    /**
     * Default Constructor : will play computer vs computer
     */
    public Game() {
        this.player1 = new Player("Computer_1", new RandomChoice());
        this.player2 = new Player("Computer_2", new RandomChoice());
        this.numberGames = 1;
    }
    /**
     * Default Constructor : will play computer vs computer numberGames times.
     */
    public Game(int numberGames) {
        this.player1 = new Player("Computer_1", new RandomChoice());
        this.player2 = new Player("Computer_2", new RandomChoice());
        this.numberGames = numberGames;
    }
    /**
     * Constructor used when a player wants to play against computer
     */
    public Game(Player player1, int numberGames) {
        this.player1 = player1;
        this.player2 = new Player("Computer", new RandomChoice());
        this.numberGames = numberGames;
    }
    /**
     * Constructor used when a player wants to play against computer
     */
    public Game(Player player1) {
        this.player1 = player1;
        this.player2 = new Player("Computer", new RandomChoice());
        this.numberGames = 1;
    }
    /**
     * Constructor used when we do not specify the number of games
     */
    public Game(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.numberGames = 1;
    }
    /**
     * Constructors used when we define the number of games
     */
    public Game(Player player1, Player player2, int numberGames) {
        this.player1 = player1;
        this.player2 = player2;
        this.numberGames = numberGames;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public int getNumberGames() {
        return numberGames;
    }

    public void setNumberGames(int numberGames) {
        this.numberGames = numberGames;
    }

    /**
     * This method will be used to play the strategy requested by the Player
     * @param movePlayer1 : the played move by player 1
     * @param movePlayer2  : the payed move by player 2
     * @return the winner firstName
     */
    private String identifyWinner(PlayerMove movePlayer1, PlayerMove movePlayer2) {
        String playerFirstName = "";
        final int[][] decisionArray = movePlayer1.getDecisionArray();
        if (decisionArray != null) {
            if (decisionArray[movePlayer1.getIndex()][movePlayer2.getIndex()] == -1) {
                playerFirstName = player2.getFirstName();
                player2.setScore(player2.getScore() + 1);
            } else if (decisionArray[movePlayer1.getIndex()][movePlayer2.getIndex()] == 1) {
                playerFirstName = player1.getFirstName();
                player1.setScore(player1.getScore() + 1);
            }
            return playerFirstName;
        }
        else{
            System.err.println("[ERROR] please, check your enum list in PlayerMove.java");
            return " ";
        }
    }
    /**
     * This method is the entrance point to the game.
     * Base on players strategy, will determine or retrieve
     * the moves played by players and identify which player wins
     */
    public ArrayList<Player> launchGame()
    {
        ArrayList<Player> playerArrayList = new ArrayList<>();
        for (int iteration = 0; iteration < numberGames; iteration++)
        {
            PlayerMove movePlayer1 = player1.playMove();
            PlayerMove movePlayer2 = player2.playMove();
            System.out.println("Player " +player1.getFirstName() + " plays : " +movePlayer1);
            System.out.println("Player " +player2.getFirstName() + " plays : " +movePlayer2);
            if(movePlayer1.equals(movePlayer2))
            {
                System.out.println(" Round " +iteration + " : No one won this round ");
            }
            else {
                System.out.println(" Round " +iteration + " : Player " + this.identifyWinner(movePlayer1, movePlayer2) + " won !");
            }
        }
        System.out.println("Scores : " +player1.getFirstName()+ " " +player1.getScore()
                            + " " +player2.getFirstName()+ " " +player2.getScore());
        playerArrayList.add(player1);
        playerArrayList.add(player2);
        return playerArrayList;
    }

}
