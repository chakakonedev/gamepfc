package Model;

import java.util.Scanner;

public class KeyboardChoice implements Strategy {

    public KeyboardChoice() {
    }

    @Override
    public PlayerMove chooseTheMove() {
        PlayerMove playerMove;
        Scanner scanner = new Scanner(System.in);
        PlayerMove.printMovesAndIndexes();
        String index;
        do {
            System.out.println("Please, enter an integer from this list : ");
            index = scanner.nextLine();
        }while (!PlayerMove.isValid(index));
        playerMove = PlayerMove.values()[Integer.parseInt(index)];
        System.out.println("You have chosen : " + playerMove);
        return playerMove;
    }
}
