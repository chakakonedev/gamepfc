package Model;

import java.util.Random;
/**
 * This enum class is used to enumerate the different moves used in the game
 */
public enum PlayerMove {

    ROCK(0),
    PAPER(1),
    SCISSORS(2);
    /*LIZARD(3),
    SPOCK(4);*/

    private static final int[][] rpsDecisionArray = {
            {0, -1, 1},
            {1, 0, -1},
            {-1, 1, 0}
    };
    private static final int[][] rpslsDecisionArray = {
            {0, -1, 1, 1, -1},
            {1, 0, -1, -1, 1},
            {-1, 1, 0, 1, -1},
            {-1, 1, -1, 0, 1},
            {1, -1, 1, -1, 0}
    };

    private int index;

    PlayerMove(int index){
        this.index = index;
    }

    public int getIndex() {
        return this.index;
    }
    /**
     * Determine the decision table to use depending on the list of enum
     * @return next player move
     */
    public final int[][] getDecisionArray(){
        if(PlayerMove.values().length == rpsDecisionArray.length)
        {
            return rpsDecisionArray;
        }
        else if(PlayerMove.values().length == rpslsDecisionArray.length){
            return rpslsDecisionArray;
        }
        return null;
    }
    /**
     * Determine the next move that will be played by this Player
     * @return next player move
     */
    public PlayerMove next() {
        return values()[(ordinal() + 1)%PlayerMove.values().length];
    }
    /**
     * Determine randomly the Player move
     * @return the player move
     */
    public static PlayerMove getRandomMove(){
        return PlayerMove.values()[new Random().nextInt(PlayerMove.values().length)];
    }

    /**
     * Print the list and indexes of the different moves
     */
    public static void printMovesAndIndexes(){
        final PlayerMove[] playerMovesList = PlayerMove.values();
        for (PlayerMove playerMove : playerMovesList)
        {
            System.out.println(playerMove.ordinal() + " --> " +playerMovesList[playerMove.ordinal()]);
        }
    }

    /**
     * Check if player enter a valid index of the game movement
     * @return true/false.
     */
    public static boolean isValid(String index) {
        if(index.chars().allMatch( Character::isDigit )){
            int indexValue = Integer.parseInt(index);
            return indexValue >= 0 && indexValue < PlayerMove.values().length;
        }
        else{
            return false;
        }
    }
}
