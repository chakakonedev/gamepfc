package Model;

public class IncrementalChoice implements Strategy{
    private PlayerMove lastMove;

    public IncrementalChoice() {
        this.lastMove = PlayerMove.ROCK;
    }
    public IncrementalChoice(PlayerMove lastMove) {
        this.lastMove = lastMove;
    }

    @Override
    public PlayerMove chooseTheMove() {
        PlayerMove currentMove = lastMove;
        this.lastMove = lastMove.next();
        return currentMove;
    }

}
