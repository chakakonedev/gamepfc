package Model;

public class Player {
    private String firstName;
    private Strategy strategy;
    private int score;

    public Player(String firstName, Strategy strategy) {
        this.firstName = firstName;
        this.strategy = strategy;
    }

    public String getFirstName() {
        return firstName;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
    /**
     * This method will be used to play the strategy requested by the Player
     * @return the player move
     */
    public PlayerMove playMove() {
        return strategy.chooseTheMove();
    }

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }
}
