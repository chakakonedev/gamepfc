package Model;

public class RandomChoice implements Strategy {
    private PlayerMove randomMove;

    public RandomChoice() {
    }

    @Override
    public PlayerMove chooseTheMove() {
        return PlayerMove.getRandomMove();
    }
}
