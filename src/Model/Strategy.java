package Model;

public interface Strategy {
    PlayerMove chooseTheMove();
}
