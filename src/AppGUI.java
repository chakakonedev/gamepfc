import Model.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

class AppGUI extends JFrame {

    private JButton buttonResultPlayer1 = new JButton();
    private JButton buttonResultPlayer2 = new JButton();
    private ArrayList<Player> playerArrayList = new ArrayList<>();

    private TextField playerNameValue1 = new TextField(" Player_1 ");
    private TextField playerNameValue2 = new TextField(" Player_2 ");

    AppGUI(){
        this.setTitle("Rock, paper, scissor and extension App");
        this.setSize(600, 700);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setResizable(true);

        JPanel jPanel = new JPanel();
        this.setContentPane(jPanel);
        jPanel.setLayout(null);

        JButton computerVsComputer = new JButton("<html><center>Game : <br>Computer vs Computer</center></html>");
        JButton playerVsComputer = new JButton("<html><center>Game : <br>Player vs Computer</center></html>");
        JButton playerVsPlayer = new JButton("<html><center>Game : <br>Player vs Player</center></html>");

        String[] rpsListMoves = {"ROCK", "PAPER", "SCISSORS"};
        String[] rpslslistMoves = {"ROCK", "PAPER", "SCISSORS", "LIZARD", "SPOCK"};

        TextArea spinnerGameNameText = new TextArea("Choose the game");
        String[] gameNameList = {"ROCK-PAPER-SCISSORS", "Rock-PAPER-SCISSORS-LIZARD-SPOCK"};
        SpinnerListModel modelGame = new SpinnerListModel(gameNameList);
        JSpinner spinnerGameName = new JSpinner(modelGame);

        TextArea numberGamesText = new TextArea("Number of games");
        TextField numberGames = new TextField("1");

        String[] strategyList = {"FixedChoice", "KeyboardChoice", "IncrementalChoice", "RandomChoice"};
        SpinnerListModel model1 = new SpinnerListModel(strategyList);
        SpinnerListModel model2 = new SpinnerListModel(strategyList);

        TextArea playerText1 = new TextArea("Player 1");
        TextArea playerName1 = new TextArea("Firstname");
        TextArea spinnerStrategyText1 = new TextArea("Choose a strategy");
        JSpinner spinnerStrategy1 = new JSpinner(model1);
        TextArea fixedChoiceValue1 = new TextArea("Choose your Fixed move");
        SpinnerListModel model3 = new SpinnerListModel(rpsListMoves);
        //SpinnerListModel model3 = new SpinnerListModel(rpslslistMoves);
        JSpinner spinnerFixedMove1 = new JSpinner(model3);
        // force the spinner to be not editable
        JFormattedTextField playerSpinnerText1 = ((JSpinner.DefaultEditor) spinnerStrategy1.getEditor()).getTextField();
        playerSpinnerText1.setEditable(false);
        JFormattedTextField playerFixedMove1 = ((JSpinner.DefaultEditor) spinnerFixedMove1.getEditor()).getTextField();
        playerFixedMove1.setEditable(false);


        TextArea playerText2 = new TextArea("Player 2");
        TextArea playerName2 = new TextArea("Firstname");
        TextArea spinnerStrategyText2 = new TextArea("Choose a strategy");
        JSpinner spinnerStrategy2 = new JSpinner(model2);
        TextArea fixedChoiceValue2 = new TextArea("Choose your Fixed move");
        SpinnerListModel model4 = new SpinnerListModel(rpsListMoves);
        //SpinnerListModel model4 = new SpinnerListModel(rpslslistMoves);
        JSpinner spinnerFixedMove2 = new JSpinner(model4);
        // force the spinner to be not editable
        JFormattedTextField playerSpinnerText2 = ((JSpinner.DefaultEditor) spinnerStrategy2.getEditor()).getTextField();
        playerSpinnerText2.setEditable(false);
        JFormattedTextField playerFixedMove2 = ((JSpinner.DefaultEditor) spinnerFixedMove2.getEditor()).getTextField();
        playerFixedMove2.setEditable(false);

        TextArea resultTest = new TextArea("Results : Score/Winner");


        //set the places of the components
        spinnerGameNameText.setBounds(20, 40, 150, 20);
        spinnerGameName.setBounds(170, 40, 250, 20);
        numberGamesText.setBounds(20, 70, 150, 20);
        numberGames.setBounds(170, 70, 150, 20);

        playerText1.setBounds(20, 100, 150, 20);
        playerName1.setBounds(20, 130, 150, 20);
        playerNameValue1.setBounds(20, 160, 150, 20);
        spinnerStrategyText1.setBounds(20, 190, 150, 20);
        spinnerStrategy1.setBounds(20,210,150,20);
        fixedChoiceValue1.setBounds(20, 240, 150, 20);
        spinnerFixedMove1.setBounds(20, 270, 150, 20);

        playerText2.setBounds(200, 100, 150, 20);
        playerName2.setBounds(200, 130, 150, 20);
        playerNameValue2.setBounds(200, 160, 150, 20);
        spinnerStrategyText2.setBounds(200, 190, 150, 20);
        spinnerStrategy2.setBounds(200,210,150,20);
        fixedChoiceValue2.setBounds(200, 240, 150, 20);
        spinnerFixedMove2.setBounds(200, 270, 150, 20);

        resultTest.setBounds(200,350,150,20);
        buttonResultPlayer1.setBounds(200,380,150,60);
        buttonResultPlayer2.setBounds(200,460,150,60);

        computerVsComputer.setBounds(20,350,150,60);
        playerVsComputer.setBounds(20,430,150,60);
        playerVsPlayer.setBounds(20,510,150,60);


        //add buttons
        /*jPanel.add(spinnerGameNameText);
        jPanel.add(spinnerGameName);*/

        jPanel.add(playerText1);
        jPanel.add(playerName1);
        jPanel.add(playerNameValue1);
        jPanel.add(spinnerStrategyText1);
        jPanel.add(spinnerStrategy1);

        jPanel.add(playerText2);
        jPanel.add(playerName2);
        jPanel.add(playerNameValue2);
        jPanel.add(spinnerStrategyText2);
        jPanel.add(spinnerStrategy2);

        jPanel.add(numberGamesText);
        jPanel.add(numberGames);

        jPanel.add(fixedChoiceValue1);
        jPanel.add(fixedChoiceValue2);
        jPanel.add(spinnerFixedMove1);
        jPanel.add(spinnerFixedMove2);

        jPanel.add(computerVsComputer);
        jPanel.add(playerVsComputer);
        jPanel.add(playerVsPlayer);

        jPanel.add(resultTest);
        jPanel.add(buttonResultPlayer1);
        jPanel.add(buttonResultPlayer2);

        //Actions on button launching the game between computer and computer
        computerVsComputer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String strNumberGames = numberGames.getText();
                if(strNumberGames.chars().allMatch( Character::isDigit )) {
                    Game game = new Game(Integer.parseInt(strNumberGames));
                    playerArrayList = game.launchGame();
                    updateResultGui(playerArrayList);

                }
                else{
                    JOptionPane.showMessageDialog(jPanel, "Enter a valid number of games. it should be digit");
                }
            }
        });

        //Actions on button launching the game between a player and computer
        playerVsComputer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String strNumberGames = numberGames.getText();
                if(strNumberGames.chars().allMatch( Character::isDigit )) {
                    String chosenStrategy = playerSpinnerText1.getText();
                    Game game = instantiateGame(chosenStrategy, strNumberGames, playerFixedMove1);
                    playerArrayList = game.launchGame();
                    updateResultGui(playerArrayList);
                }else{
                    JOptionPane.showMessageDialog(jPanel, "Enter a valid number of games. it should be digit");
                }
            }
        });

        playerVsPlayer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String strNumberGames = numberGames.getText();
                if(strNumberGames.chars().allMatch( Character::isDigit )) {
                    String chosenStrategy1 = playerSpinnerText1.getText();
                    String chosenStrategy2 = playerSpinnerText2.getText();
                    Game game = instantiateGame(chosenStrategy1, chosenStrategy2, strNumberGames, playerFixedMove1, playerFixedMove2);
                    playerArrayList = game.launchGame();
                    updateResultGui(playerArrayList);
                }else{
                    JOptionPane.showMessageDialog(jPanel, "Enter a valid number of games. it should be digit");
                }
            }
        });
        // show all jpannel components at starting step
        jPanel.repaint();
    }
    /**
     * This method aims to instantiate the game when a player wants to player against computer
     * Based on the player strategy, it will create a Game instance
     * @param chosenStrategy : Player strategy
     * @param strNumberGames : number of times, we want to play
     * @param playerFixedMove1 : the text object containing the player move in case of fixed strategy
     * @return an instance of Game
     */
    private Game instantiateGame(String chosenStrategy, String strNumberGames, JFormattedTextField playerFixedMove1) {
        Game game;
        if(chosenStrategy.matches("FixedChoice")){
            FixedChoice fixedChoice;
            if(playerFixedMove1.getText().matches("ROCK")){
                fixedChoice = new FixedChoice(PlayerMove.ROCK);
            }
            else if(playerFixedMove1.getText().matches("PAPER"))
            {
                fixedChoice = new FixedChoice(PlayerMove.PAPER);
            }
            else
            {
                fixedChoice = new FixedChoice(PlayerMove.SCISSORS);
            }
            game = new Game(new Player(playerNameValue1.getText(), fixedChoice), Integer.parseInt(strNumberGames));
        }
        else if(chosenStrategy.matches("KeyboardChoice"))
        {
            game = new Game(new Player(playerNameValue1.getText(), new FixedChoice(PlayerMove.ROCK)),
                    Integer.parseInt(strNumberGames));
        }
        else if (chosenStrategy.matches("IncrementalChoice"))
        {
            game = new Game(new Player(playerNameValue1.getText(), new IncrementalChoice()),
                    Integer.parseInt(strNumberGames));
        }
        else {
            game = new Game(new Player(playerNameValue1.getText(), new RandomChoice()),
                    Integer.parseInt(strNumberGames));
        }
        return game;
    }
    /**
     * This method aims to instantiate a FixedChoice
     * Based on the move chosen by the player , it will create a FixedChoice instance
     * @param playerFixedMove : the text object containing the player move
     * @return an instance of FixedChoice
     */
    private FixedChoice setPlayerFixedStrategy(JFormattedTextField playerFixedMove){
        FixedChoice fixedChoice;
        if(playerFixedMove.getText().matches("ROCK")){
            fixedChoice = new FixedChoice(PlayerMove.ROCK);
        }
        else if(playerFixedMove.getText().matches("PAPER"))
        {
            fixedChoice = new FixedChoice(PlayerMove.PAPER);
        }
        else
        {
            fixedChoice = new FixedChoice(PlayerMove.SCISSORS);
        }
        return fixedChoice;
    }
    /**
     * This method aims to instantiate the game. It overrides the previous one
     * when a player_1 wants to player against player_2
     * Based on the player strategy, it will create a Game instance
     * @param chosenStrategy1 : Player_1 strategy
     * @param chosenStrategy2 : Player_2 strategy
     * @param strNumberGames : number of times, we want to play
     * @param playerFixedMove1 : the text object containing the player_1 move in case of fixed strategy
     * @param playerFixedMove2 : the text object containing the player_2 move in case of fixed strategy
     * @return an instance of Game
     */
    private Game instantiateGame(String chosenStrategy1, String chosenStrategy2, String strNumberGames, JFormattedTextField playerFixedMove1, JFormattedTextField playerFixedMove2) {
        Game game;
        if(chosenStrategy1.matches("FixedChoice") && chosenStrategy2.matches("FixedChoice")){
            FixedChoice fixedChoice1 = setPlayerFixedStrategy(playerFixedMove1);
            FixedChoice fixedChoice2 = setPlayerFixedStrategy(playerFixedMove2);

            game = new Game(new Player(playerNameValue1.getText(), fixedChoice1),
                            new Player(playerNameValue2.getText(), fixedChoice2),
                    Integer.parseInt(strNumberGames));
        }
        else if(chosenStrategy1.matches("FixedChoice") && chosenStrategy2.matches("IncrementalChoice")){
            FixedChoice fixedChoice1 = setPlayerFixedStrategy(playerFixedMove1);
            game = new Game(new Player(playerNameValue1.getText(), fixedChoice1),
                    new Player(playerNameValue2.getText(), new IncrementalChoice()),
                    Integer.parseInt(strNumberGames));
        }
        else if(chosenStrategy1.matches("IncrementalChoice") && chosenStrategy2.matches("FixedChoice")){
            FixedChoice fixedChoice1 = setPlayerFixedStrategy(playerFixedMove2);
            game = new Game(new Player(playerNameValue1.getText(), new IncrementalChoice()),
                    new Player(playerNameValue2.getText(), fixedChoice1),
                    Integer.parseInt(strNumberGames));
        }
        else if(chosenStrategy1.matches("FixedChoice") && chosenStrategy2.matches("RandomChoice")){
            FixedChoice fixedChoice1 = setPlayerFixedStrategy(playerFixedMove1);
            game = new Game(new Player(playerNameValue1.getText(), fixedChoice1),
                    new Player(playerNameValue2.getText(), new RandomChoice()),
                    Integer.parseInt(strNumberGames));
        }
        else if(chosenStrategy1.matches("RandomChoice") && chosenStrategy2.matches("FixedChoice")){
            FixedChoice fixedChoice1 = setPlayerFixedStrategy(playerFixedMove2);
            game = new Game(new Player(playerNameValue1.getText(), new RandomChoice()),
                    new Player(playerNameValue2.getText(), fixedChoice1),
                    Integer.parseInt(strNumberGames));
        }
        else if(chosenStrategy1.matches("FixedChoice") && chosenStrategy2.matches("KeyboardChoice")){
            FixedChoice fixedChoice1 = setPlayerFixedStrategy(playerFixedMove1);
            game = new Game(new Player(playerNameValue1.getText(), fixedChoice1),
                    new Player(playerNameValue2.getText(), new KeyboardChoice()),
                    Integer.parseInt(strNumberGames));
        }
        else if(chosenStrategy1.matches("KeyboardChoice") && chosenStrategy2.matches("FixedChoice")){
            FixedChoice fixedChoice1 = setPlayerFixedStrategy(playerFixedMove2);
            game = new Game(new Player(playerNameValue1.getText(), new KeyboardChoice()),
                    new Player(playerNameValue2.getText(), fixedChoice1),
                    Integer.parseInt(strNumberGames));
        }
        else if(chosenStrategy1.matches("KeyboardChoice") && chosenStrategy2.matches("KeyboardChoice"))
        {
            game = new Game(new Player(playerNameValue1.getText(), new KeyboardChoice()),
                    new Player(playerNameValue2.getText(), new KeyboardChoice()),
                    Integer.parseInt(strNumberGames));
        }
        else if(chosenStrategy1.matches("KeyboardChoice") && chosenStrategy2.matches("IncrementalChoice"))
        {
            game = new Game(new Player(playerNameValue1.getText(), new KeyboardChoice()),
                    new Player(playerNameValue2.getText(), new IncrementalChoice()),
                    Integer.parseInt(strNumberGames));
        }
        else if(chosenStrategy1.matches("IncrementalChoice") && chosenStrategy2.matches("KeyboardChoice"))
        {
            game = new Game(new Player(playerNameValue1.getText(), new IncrementalChoice()),
                    new Player(playerNameValue2.getText(), new IncrementalChoice()),
                    Integer.parseInt(strNumberGames));
        }
        else if(chosenStrategy1.matches("KeyboardChoice") && chosenStrategy2.matches("RandomChoice"))
        {
            game = new Game(new Player(playerNameValue1.getText(), new KeyboardChoice()),
                    new Player(playerNameValue2.getText(), new RandomChoice()),
                    Integer.parseInt(strNumberGames));
        }
        else if(chosenStrategy1.matches("RandomChoice") && chosenStrategy2.matches("KeyboardChoice"))
        {
            game = new Game(new Player(playerNameValue1.getText(), new RandomChoice()),
                    new Player(playerNameValue2.getText(), new KeyboardChoice()),
                    Integer.parseInt(strNumberGames));
        }
        else if (chosenStrategy1.matches("IncrementalChoice") && chosenStrategy2.matches("IncrementalChoice"))
        {
            game = new Game(new Player(playerNameValue1.getText(), new IncrementalChoice()),
                    new Player(playerNameValue2.getText(), new IncrementalChoice()),
                    Integer.parseInt(strNumberGames));
        }
        else if (chosenStrategy1.matches("IncrementalChoice") && chosenStrategy2.matches("RandomChoice"))
        {
            game = new Game(new Player(playerNameValue1.getText(), new IncrementalChoice()),
                    new Player(playerNameValue2.getText(), new RandomChoice()),
                    Integer.parseInt(strNumberGames));
        }
        else if (chosenStrategy1.matches("RandomChoice") && chosenStrategy2.matches("IncrementalChoice"))
        {
            game = new Game(new Player(playerNameValue1.getText(), new RandomChoice()),
                    new Player(playerNameValue2.getText(), new IncrementalChoice()),
                    Integer.parseInt(strNumberGames));
        }
        else {
            // random random will be used as the default scenario in case of some missing
            // (chosenStrategy1.matches("RandomChoice") && chosenStrategy2.matches("RandomChoice"))
            game = new Game(new Player(playerNameValue1.getText(), new RandomChoice()),
                    new Player(playerNameValue2.getText(), new RandomChoice()),
                    Integer.parseInt(strNumberGames));
        }
        return game;
    }
    /**
     * This method aims to display players information (firstname, score) and to update the
     * color (winner = green, looser = red, equality = gray)
     * @param playerArrayList : list of players
     */
    private void updateResultGui(ArrayList<Player> playerArrayList)
    {
        buttonResultPlayer1.setText(playerArrayList.get(0).getFirstName() + " : " +playerArrayList.get(0).getScore());
        buttonResultPlayer2.setText(playerArrayList.get(1).getFirstName() + " : " +playerArrayList.get(1).getScore());

        if(playerArrayList.get(0).getScore() == playerArrayList.get(1).getScore())
        {
            buttonResultPlayer1.setBackground(Color.GRAY);
            buttonResultPlayer2.setBackground(Color.GRAY);
        }
        else if(playerArrayList.get(0).getScore() > playerArrayList.get(1).getScore())
        {
            buttonResultPlayer1.setBackground(Color.GREEN);
            buttonResultPlayer2.setBackground(Color.red);
        }
        else
        {
            buttonResultPlayer1.setBackground(Color.red);
            buttonResultPlayer2.setBackground(Color.GREEN);
        }
    }

}
