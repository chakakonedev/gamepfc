# README #

### What is this repository for? ###

This repo is a project which aims to program the ROCK, PAPER, SCISSORS game.
There is 2 versions possible with this dev. ROCK, PAPER, SCISSORS game
or the extended one ROCK, PAPER, SCISSORS, LIZARD, SPOCK.

I have decided to implements this in Java and will try to do the same thing in Scala later to get familiarize with this language.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

I have develop it using Java with Intellij IDE. So if you have the same idea, it is perfect, otherwise use your preferred IDE

This project uses jdk12 and Junit for Unit tests. The project has been configured with 2 configurations (main and JunitTests).

* How to open the project
1. After having cloned this repo, mode inside the directory.
2. Open Intellij
2. select, file --> open --> and select this project.
* Run the App
When your project is opened, you can launch the Main configuration which will open GUI (very basic) aiming you to test different scenario of the game
There are also some functional tests, which was here for me during the development phase to test the overall functioning in addition to the Junit test. I keep it,
You can also choose the JunitTests configuration to run those tests.

### Possible improvements ###
RPS : Rock, paper, scissors

RPSLS : Rock, paper, scissors, lizard, spock
* Automatize the choose of the game version RPS or RPSLS. 
With these dev, by default player will always plays with the ROCK, PAPER, SCISSORS game.
It he wants to play to ROCK, PAPER, SCISSORS, LIZARD, SPOCK game, he has to change the list of enum class
in PlayerMove.java (see lines 12, 13, 14) and also to change AppGUI.java that the spinner will be filled 
with rpslslistMoves list instead of rpslistMoves (see lines 35, 55, 70).

To test the 2 versions of the game, one can extract the commit doing this test (RPS or RPSLS).

Another solution is to provide 2 versions of this app (bad solution in term of user satisfaction).

* Improve the GUI
After having seen this GUI implemented, everyone will agree that the GUI needs further works (Fill free to contribute).
I have done only a demo version

### Contribution guidelines ###
Feel free to contribute to this project.
1. Create a new branch
2. Write your tests
3. Implements your contributions
4. Test again the application
5. Create a Pull requests

### Who do I talk to? ###

I would be very glad to discuss about your solution and mine to build this game app. Feel free 
to contact me : Chaka KONE (https://www.linkedin.com/in/chaka-kone-a082a883/)
